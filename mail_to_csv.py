import os
import shutil
import time
import traceback
import openpyxl
from imap_tools import MailBox, AND, OR, NOT, A, H, U
import constants
import telebot
import datetime
from zipfile import ZipFile
from pathlib import Path


bot = telebot.TeleBot(constants.token)


def send_message(message):
    bot.send_message(constants.admin_tg, message)


def already_processed(filename):
    try:
        with open(constants.log, mode='r', encoding='utf-8') as f:
            line_list = [line.rstrip('\n') for line in f]
        return True if filename in line_list else False
    except FileNotFoundError:
        return False


def add_to_log(filename):
    with open(constants.log, mode='a', encoding='utf-8') as f:
        f.write(filename + '\n')


def parse_xls(sh, first, fop):
    lines = []
    for i in range(first, sh.max_row + 1):
        if str(sh.cell(i, constants.npp_clmn).value).isdecimal():
            lines.append(f'{sh.cell(i, 9).value};{constants.fops[fop]};{sh.cell(i, 4).value};'
                         f'{sh.cell(i, 5).value};{sh.cell(i, 3).value};{constants.edrpou_np}\n')
    if lines:
        with open(Path(constants.out_path)/'file.csv', 'a', encoding='utf-8') as f:
            f.writelines(lines)
    return True


def find_first_row(sh):
    for i in range(1, sh.max_row + 1):
        if sh.cell(i, constants.npp_clmn).value == 1:
            return i


def define_fop(file_name):
    for fop in constants.fops:
        if fop in file_name:
            return fop
    return None


def check_columns(sh, row):
    for column in constants.get_clmn:
        if sh.cell(row, column).value != constants.get_clmn[column]:
            return False
    return True


def remove_dir(dir: str | Path) -> None:
    # shutil.rmtree(temp_path, ignore_errors=True)
    if isinstance(dir, str):
        dir = Path(dir)
    for file in dir.glob('**/*'):
        if file.is_file():
            file.unlink()
    for folder in sorted(dir.glob('**/*'), key=lambda x: len(str(x)), reverse=True):
        folder.rmdir()
    dir.rmdir()


def set_work_dir():
    os.chdir(Path(__file__).absolute().parent)


def repack_xls(file: Path) -> None:
    temp_path = file.parent/'tmp'
    temp_path.mkdir(exist_ok=True)
    ZipFile(file).extractall(temp_path)
    (temp_path/'xl'/'SharedStrings.xml').rename(target=temp_path/'xl'/'sharedStrings.xml')
    shutil.make_archive(str(file.absolute()), 'zip', temp_path)
    file.with_suffix(file.suffix + '.zip').replace(target=file)
    remove_dir(temp_path)


def process_file(fop, file: Path):
    repack_xls(file)

    wb = openpyxl.load_workbook(file)
    sh = wb.active
    for items in sorted(sh.merged_cells.ranges):
        sh.unmerge_cells(str(items))

    first = find_first_row(sh)
    if not first:
        send_message(f'{__file__}\nНе найден первый ряд в файле {file}')
        raise Exception('Не найден первый ряд в файле')

    if not check_columns(sh, first - 2):
        send_message(f'{__file__}\nНе пройдена проверка на столбцы в файле {file}')
        raise Exception('Не пройдена проверка на столбцы в файле')

    if parse_xls(sh=sh, first=first, fop=fop):
        return True


def process_mail(mail):
    xls_path = Path.cwd()/'_xls'
    xls_path.mkdir(exist_ok=True)

    from_date = datetime.date.today() - datetime.timedelta(constants.days_ago)
    with MailBox(mail['server']).login(mail['login'], mail['password'], 'INBOX') as mailbox:
        for msg in mailbox.fetch(AND(date_gte=datetime.date(from_date.year, from_date.month, from_date.day))):
            for att in msg.attachments:
                filename = att.filename.replace(':', '').replace('/', '').replace('\\', '')
                fop = define_fop(filename)
                if fop and 'xls' in filename.lower() and not already_processed(filename):
                    print('--- Start processing ---:', filename)
                    (Path.cwd()/xls_path/filename).write_bytes(att.payload)
                    if process_file(fop, Path.cwd()/xls_path/filename):
                        add_to_log(filename)
                        print('--- Successfully processed and added to log---:', filename)
                else:
                    print('--- Skipping---:', filename)
    remove_dir(xls_path)


if __name__ == '__main__':
    try:
        set_work_dir()
        for mail in constants.mail_boxes:
            process_mail(mail)
    except Exception as e:
        send_message(f"Ошибка в программе {__file__}\n{str(e)}\n{traceback.format_exc()}")


